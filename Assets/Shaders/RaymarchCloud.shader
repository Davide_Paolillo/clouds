﻿Shader "Unlit/RaymarchCloud"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 position : SV_POSITION;
                float3 cameraViewDirection: TEXCOORD1;
            };

            sampler2D _MainTex;

            // Shape settings
            float scale;
            float3 shapeOffset;
            float detailNoiseScale;
            float detailNoiseWeight;
            float3 detailWeights;
            float3 detailOffset;
            float4 shapeNoiseWeights;
            float4 phaseParams;

            // Ray march steps
            int numberOfSteps;
            float rayOffsetStrength;

            // Light march steps
            int numberOfStepsLight;

            // Light settings
            float lightAbsorptionTowardSun;
            float lightAbsorptionThroughCloud;
            float darknessThreshold;
            float4 _LightColor0;
            float4 colA;
            float4 colB;

            // Animation settings
            float timeScale;
            float baseSpeed;
            float detailSpeed;


            float3 boundsMin;
            float3 boundsMax;
            UNITY_DECLARE_DEPTH_TEXTURE(_CameraDepthTexture);

            Texture3D<float4> NoiseTexture;
            SamplerState samplerNoiseTexture;

            Texture3D<float4> NoiseTextureLowResolution;
            SamplerState samplerNoiseTextureLowResolution;

            Texture2D<float4> BlueNoise;
            SamplerState samplerBlueNoise;

            float3 CloudOffset;
            float CloudScale;
            float DensityThreshold;
            float DensityMultiplier;
            float densityOffset;

            float2 boxDistance (float3 rayOrigin, float3 invRaydir, float3 minBound, float3 maxBound)
            {
                // Adapted from: http://jcgt.org/published/0007/03/04
                float3 t0 = (minBound - rayOrigin) * invRaydir;
                float3 t1 = (maxBound - rayOrigin) * invRaydir;
                float3 tmin = min(t0, t1);
                float3 tmax = max(t0, t1);
                
                float dstA = max(max(tmin.x, tmin.y), tmin.z);
                float dstB = min(tmax.x, min(tmax.y, tmax.z));

                // CASE 1: ray intersects box from outside (0 <= dstA <= dstB)
                // dstA is dst to nearest intersection, dstB dst to far intersection

                // CASE 2: ray intersects box from inside (dstA < 0 < dstB)
                // dstA is the dst to intersection behind the ray, dstB is dst to forward intersection

                // CASE 3: ray misses box (dstA > dstB)

                float dstToBox = max(0, dstA);
                float dstInsideBox = max(0, dstB - dstToBox);
                return float2(dstToBox, dstInsideBox);
            }

            v2f vert (appdata v)
            {
                v2f o;
                o.position = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                // Camera space matches OpenGL convention where cam forward is -z. In unity forward is positive z.
                // (https://docs.unity3d.com/ScriptReference/Camera-cameraToWorldMatrix.html)
                float3 viewVector = mul(unity_CameraInvProjection, float4(v.uv * 2 - 1, 0, -1));
                o.cameraViewDirection = mul(unity_CameraToWorld, float4(viewVector,0));

                return o;
            }
            
            float2 squareUV(float2 uv) 
            {
                float width = _ScreenParams.x;
                float height =_ScreenParams.y;
                //float minDim = min(width, height);
                float s = 1000;
                float x = uv.x * width;
                float y = uv.y * height;
                return float2 (x/s, y/s);
            }

            float remap(float v, float minOld, float maxOld, float minNew, float maxNew) {
                return minNew + (v-minOld) * (maxNew - minNew) / (maxOld-minOld);
            }

            // Henyey-Greenstein
            float henyeyGreenstein(float a, float g) 
            {
                float g2 = g*g;
                return (1-g2) / (4*3.1415*pow(1+g2-2*g*(a), 1.5));
            }

            float phase(float a) 
            {
                float blend = .5;
                float hgBlend = henyeyGreenstein(a,phaseParams.x) * (1-blend) + henyeyGreenstein(a,-phaseParams.y) * blend;
                return phaseParams.z + hgBlend*phaseParams.w;
            }

            float sampleDensity (float3 position)
            {
                float3 uvw = position * CloudScale * 0.001 + CloudOffset * 0.001;
                float4 shape = NoiseTexture.SampleLevel(samplerNoiseTexture, uvw, 0);
                float density = max(0, shape.b - DensityThreshold) * DensityMultiplier;

                return density;
            }

            float sampleDensityComplex (float3 position)
            {
                // Constants:
                const int mipLevel = 0;
                const float baseScale = 1/1000.0;
                const float offsetSpeed = 1/100.0;

                // Calculate texture sample positions
                float time = _Time.x * timeScale;
                float3 size = boundsMax - boundsMin;
                float3 boundsCentre = (boundsMin+boundsMax) * .5;
                float3 uvw = (size * .5 + position) * baseScale * scale;
                float3 shapeSamplePos = uvw + shapeOffset * offsetSpeed + float3(time,time*0.1,time*0.2) * baseSpeed;

                // Calculate falloff at along x/z edges of the cloud container
                const float containerEdgeFadeDst = 50;
                float dstFromEdgeX = min(containerEdgeFadeDst, min(position.x - boundsMin.x, boundsMax.x - position.x));
                float dstFromEdgeZ = min(containerEdgeFadeDst, min(position.z - boundsMin.z, boundsMax.z - position.z));
                float edgeWeight = min(dstFromEdgeZ,dstFromEdgeX)/containerEdgeFadeDst;

                float gMin = .2;
                float gMax = .7;
                float heightPercent = (position.y - boundsMin.y) / size.y;
                float heightGradient = saturate(remap(heightPercent, 0.0, gMin, 0, 1)) * saturate(remap(heightPercent, 1, gMax, 0, 1));
                heightGradient *= edgeWeight;

                // Calculate base shape density
                float4 shapeNoise = NoiseTexture.SampleLevel(samplerNoiseTexture, shapeSamplePos, mipLevel);
                float4 normalizedShapeWeights = shapeNoiseWeights / dot(shapeNoiseWeights, 1);
                float shapeFBM = dot(shapeNoise, normalizedShapeWeights) * heightGradient;
                float baseShapeDensity = shapeFBM + densityOffset * .1;

                // Save sampling from detail tex if shape density <= 0
                if (baseShapeDensity > 0) 
                {
                    // Sample detail noise
                    float3 detailSamplePos = uvw * detailNoiseScale + detailOffset * offsetSpeed + float3(time*.4,-time,time*0.1) * detailSpeed;
                    float4 detailNoise = NoiseTextureLowResolution.SampleLevel(samplerNoiseTextureLowResolution, detailSamplePos, mipLevel);
                    float3 normalizedDetailWeights = detailWeights / dot(detailWeights, 1);
                    float detailFBM = dot(detailNoise, normalizedDetailWeights);

                    // Subtract detail noise from base shape (weighted by inverse density so that edges get eroded more than centre)
                    float oneMinusShape = 1 - shapeFBM;
                    float detailErodeWeight = oneMinusShape * oneMinusShape * oneMinusShape;
                    float cloudDensity = baseShapeDensity - (1-detailFBM) * detailErodeWeight * detailNoiseWeight;
    
                    return cloudDensity * DensityMultiplier * 0.1;
                }

                return 0;
            }

            // Calculate proportion of light that reaches the given point from the lightsource
            float3 lightMarch(float3 position)
            {
                float3 lightDirection = _WorldSpaceLightPos0.xyz;
                float distance = boxDistance(position, 1/lightDirection, boundsMin, boundsMax).y;

                float stepSize = distance / numberOfStepsLight;
                float totalDensity = 0;

                for (int i = 0; i < numberOfStepsLight; i++)
                {
                    position += lightDirection * stepSize;
                    totalDensity += max(0, sampleDensityComplex(position) * stepSize);
                }

                // How much light is passing through the sun
                float transmittance = exp(-totalDensity * lightAbsorptionTowardSun);

                return darknessThreshold + (1-darknessThreshold) * transmittance;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                //fixed4 col = tex2D(_MainTex, i.uv);
                float3 cameraRayOrigin = _WorldSpaceCameraPos;
                float3 cameraRayDirection = normalize(i.cameraViewDirection);

                float nonLinearDepth = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, i.uv);
                float depth = LinearEyeDepth(nonLinearDepth) * length(i.cameraViewDirection);

                float2 intersectBox = boxDistance(cameraRayOrigin, 1/cameraRayDirection, boundsMin, boundsMax);
                float distanceToBox = intersectBox.x;
                float distanceFromInsideBox = intersectBox.y;

                // random starting offset (makes low-res results noisy rather than jagged/glitchy, which is nicer)
                float randomOffset = BlueNoise.SampleLevel(samplerBlueNoise, squareUV(i.uv*3), 0);
                randomOffset *= rayOffsetStrength;

                // point of intersection with the cloud container
                float3 entryPoint = cameraRayOrigin + cameraRayDirection * distanceToBox;

                float distanceTravelled = randomOffset;
                float stepSize = distanceFromInsideBox / numberOfSteps;
                float distanceLimit = min(depth - distanceFromInsideBox, distanceFromInsideBox);

                 // Phase function makes clouds brighter around sun
                float cosAngle = dot(cameraRayDirection, _WorldSpaceLightPos0.xyz);
                float phaseVal = phase(cosAngle);

                float transmittance = 1; // We start with full transparency 
                float3 lightEnergy = 0; // We start with no accumulated light

                float totalDensity = 0;

                // Marching inside the volume
                while (distanceTravelled < distanceLimit)
                {
                    float3 rayPosition = entryPoint + cameraRayDirection * distanceTravelled;

                    float density = sampleDensityComplex(rayPosition);
                    
                    // Compute the scattered light
                    
                    // If we find a cloud then we lightmarch through it
                    if (density > 0)
                    {
                        float lightTransmittance = lightMarch(rayPosition);
                        lightEnergy += density * stepSize * transmittance * lightTransmittance * phaseVal;
                        transmittance *= exp(-density * stepSize * lightAbsorptionThroughCloud);

                        // Exit early if T is close to zero as further samples won't affect the result much
                        if (transmittance < 0.01f) {
                            break;
                        }
                    }
                    //totalDensity += density * stepSize;
                    distanceTravelled += stepSize;
                }
                
                // This is the quantity of light that reaches us viewrs
                //float transmittance = exp(-totalDensity);
                
                float3 backgroundCol = tex2D(_MainTex,i.uv);
                float3 cloudCol = lightEnergy * _LightColor0;
                float3 col = backgroundCol * transmittance + cloudCol;

                return float4(col, 0);
            }
            ENDCG
        }
    }
}
