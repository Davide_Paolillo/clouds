﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseGeneratorComputeShader : MonoBehaviour
{
    [SerializeField] private ComputeShader computeShader;
    [SerializeField] private ComputeShader computeShader3D;
    [SerializeField] private ComputeShader slicer;

    [Range(0, 896)][SerializeField] private int textureResolution = 128;

    [SerializeField] private int numberOfSamplePoints = 10;

    [SerializeField] private float maxDistance = 30f;

    private RenderTexture target;

    private ComputeBuffer computeBuffer;
    private ComputeBuffer computeBufferGreen;
    private ComputeBuffer computeBufferBlue;
    private ComputeBuffer computeBufferAlpha;

    private Vector2Int[] randomPoints;
    private Vector3Int[] randomPoints3DRed;
    private Vector3Int[] randomPoints3DGreen;
    private Vector3Int[] randomPoints3DBlue;
    private Vector3Int[] randomPoints3DAlpha;

    public RenderTexture Target { get => target; }
    public int TextureResolution { get => textureResolution; }
    public ComputeShader Slicer { get => slicer; }

    public RenderTexture GenerateTexture3D(Vector3 scale, ResolutionDownscale lowerResolutionFactor = ResolutionDownscale.ZERO)
    {
        int tmp = textureResolution;
        textureResolution = Mathf.RoundToInt(textureResolution / (int)lowerResolutionFactor);

        var format = UnityEngine.Experimental.Rendering.GraphicsFormat.R16G16B16A16_UNorm;
        target = new RenderTexture(textureResolution, textureResolution, 0, RenderTextureFormat.ARGB32, 1);
        target.enableRandomWrite = true;
        target.volumeDepth = textureResolution;
        target.graphicsFormat = format;
        target.dimension = UnityEngine.Rendering.TextureDimension.Tex3D;
        target.filterMode = FilterMode.Bilinear;
        target.wrapMode = TextureWrapMode.Repeat;
        target.Create();

        computeShader3D.SetFloat("scaleX", scale.x);
        computeShader3D.SetFloat("scaleY", scale.z);
        computeShader3D.SetFloat("scaleZ", scale.y);

        SetComputeBuffer3D();
        SetShader3DParameters();

        int noiseGeneratorKernell = computeShader3D.FindKernel("NoiseGenerator3D");
        computeShader3D.SetTexture(noiseGeneratorKernell, "Result", target);

        int threadGroupX = textureResolution / 8;
        int threadGroupY = textureResolution / 8;
        int threadGroupZ = textureResolution / 8;
        computeShader3D.Dispatch(noiseGeneratorKernell, threadGroupX, threadGroupY, threadGroupZ);

        computeBuffer.Dispose();
        computeBufferGreen.Dispose();
        computeBufferBlue.Dispose();
        computeBufferAlpha.Dispose();
        computeBuffer = null;
        computeBufferGreen = null;
        computeBufferBlue = null;
        computeBufferAlpha = null;

        textureResolution = tmp;

        return target;
    }

    public RenderTexture GenerateTexture3D(ResolutionDownscale lowerResolutionFactor = ResolutionDownscale.ZERO)
    {
        int tmp = textureResolution;
        textureResolution = Mathf.RoundToInt(textureResolution / (int)lowerResolutionFactor);

        var format = UnityEngine.Experimental.Rendering.GraphicsFormat.R16G16B16A16_UNorm;
        target = new RenderTexture(textureResolution, textureResolution, 0, RenderTextureFormat.ARGB32, 1);
        target.enableRandomWrite = true;
        target.volumeDepth = textureResolution;
        target.graphicsFormat = format;
        target.dimension = UnityEngine.Rendering.TextureDimension.Tex3D;
        target.filterMode = FilterMode.Bilinear;
        target.wrapMode = TextureWrapMode.Repeat;
        target.Create();

        computeShader3D.SetFloat("scaleX", 1f);
        computeShader3D.SetFloat("scaleY", 1f);
        computeShader3D.SetFloat("scaleZ", 1f);

        SetComputeBuffer3D();
        SetShader3DParameters();

        int noiseGeneratorKernell = computeShader3D.FindKernel("NoiseGenerator3D");
        computeShader3D.SetTexture(noiseGeneratorKernell, "Result", target);

        int threadGroupX = textureResolution / 8;
        int threadGroupY = textureResolution / 8;
        int threadGroupZ = textureResolution / 8;
        computeShader3D.Dispatch(noiseGeneratorKernell, threadGroupX, threadGroupY, threadGroupZ);

        computeBuffer.Dispose();
        computeBufferGreen.Dispose();
        computeBufferBlue.Dispose();
        computeBufferAlpha.Dispose();
        computeBuffer = null;
        computeBufferGreen = null;
        computeBufferBlue = null;
        computeBufferAlpha = null;

        textureResolution = tmp;

        return target;
    }

    public RenderTexture GenerateTexture2D(Vector3 scale)
    {
        target = new RenderTexture(textureResolution, textureResolution, 0, RenderTextureFormat.ARGB32, 1);
        target.enableRandomWrite = true;
        target.dimension = UnityEngine.Rendering.TextureDimension.Tex2D;
        target.useMipMap = false;
        target.Create();

        computeShader.SetFloat("scaleX", scale.x);
        computeShader.SetFloat("scaleY", scale.z);
        computeShader.SetFloat("scaleZ", scale.y);

        SetComputeBuffer();
        SetShaderParameters();

        int noiseGeneratorKernell = computeShader.FindKernel("NoiseGenerator");
        computeShader.SetTexture(noiseGeneratorKernell, "Result", target);

        int threadGroupX = textureResolution / 8;
        int threadGroupY = textureResolution / 8;
        int threadGroupZ = 1;
        computeShader.Dispatch(noiseGeneratorKernell, threadGroupX, threadGroupY, threadGroupZ);

        computeBuffer.Dispose();
        computeBuffer = null;

        return target;
    }

    public RenderTexture GenerateTexture2D()
    {
        target = new RenderTexture(textureResolution, textureResolution, 0, RenderTextureFormat.ARGB32, 1);
        target.enableRandomWrite = true;
        target.dimension = UnityEngine.Rendering.TextureDimension.Tex2D;
        target.useMipMap = false;
        target.Create();

        computeShader.SetFloat("scaleX", 1f);
        computeShader.SetFloat("scaleY", 1f);
        computeShader.SetFloat("scaleZ", 1f);

        SetComputeBuffer();
        SetShaderParameters();

        int noiseGeneratorKernell = computeShader.FindKernel("NoiseGenerator");
        computeShader.SetTexture(noiseGeneratorKernell, "Result", target);

        int threadGroupX = textureResolution / 8;
        int threadGroupY = textureResolution / 8;
        int threadGroupZ = 1;
        computeShader.Dispatch(noiseGeneratorKernell, threadGroupX, threadGroupY, threadGroupZ);

        computeBuffer.Dispose();
        computeBuffer = null;

        return target;
    }

    public List<RenderTexture> GenerateTextures2D(Vector3 scale)
    {
        List<RenderTexture> renderTextures = new List<RenderTexture>();

        for (int i = 0; i < textureResolution; i++)
        {
            computeShader.SetInt("offset", i);
            renderTextures.Add(GenerateTexture2D(scale));
        }

        return renderTextures;
    }

    public List<RenderTexture> GenerateTextures2D()
    {
        List<RenderTexture> renderTextures = new List<RenderTexture>();

        for (int i = 0; i < textureResolution; i++)
        {
            computeShader.SetInt("offset", i);
            renderTextures.Add(GenerateTexture2D());
        }

        return renderTextures;
    }

    private void SetShaderParameters()
    {
        int noiseGeneratorKernell = computeShader.FindKernel("NoiseGenerator");

        computeShader.SetFloat("width", textureResolution);
        computeShader.SetFloat("height", textureResolution);
        computeShader.SetFloat("depth", textureResolution);

        computeShader.SetInt("numberOfSamplePoints", numberOfSamplePoints);

        computeShader.SetFloat("maxDistance", maxDistance);

        computeShader.SetBuffer(noiseGeneratorKernell, "samplePoints", computeBuffer);
        computeShader.SetInt("numberOfSamplePoints", numberOfSamplePoints);
        computeShader.SetInt("textureResolution", textureResolution);
    }

    private void SetShader3DParameters()
    {
        int noiseGeneratorKernell = computeShader3D.FindKernel("NoiseGenerator3D");

        computeShader3D.SetFloat("width", textureResolution);
        computeShader3D.SetFloat("height", textureResolution);
        computeShader3D.SetFloat("depth", textureResolution);

        computeShader3D.SetInt("numberOfSamplePoints", numberOfSamplePoints);

        computeShader3D.SetFloat("maxDistance", maxDistance);

        computeShader3D.SetBuffer(noiseGeneratorKernell, "samplePoints", computeBuffer);
        computeShader3D.SetBuffer(noiseGeneratorKernell, "samplePointsG", computeBufferGreen);
        computeShader3D.SetBuffer(noiseGeneratorKernell, "samplePointsB", computeBufferBlue);
        computeShader3D.SetBuffer(noiseGeneratorKernell, "samplePointsA", computeBufferAlpha);
        computeShader3D.SetInt("numberOfSamplePoints", numberOfSamplePoints);
        computeShader3D.SetInt("textureResolution", textureResolution);
    }

    private void SetComputeBuffer()
    {
        if (randomPoints == null || randomPoints.Length != numberOfSamplePoints * textureResolution)
            InitializeRandomPoints();

        computeBuffer = new ComputeBuffer(randomPoints.Length, 8);
        computeBuffer.SetData(randomPoints);
    }

    private void SetComputeBuffer3D()
    {
        if (randomPoints == null || randomPoints.Length != numberOfSamplePoints * textureResolution * textureResolution)
            InitializeRandomPoints3D();

        computeBuffer = new ComputeBuffer(randomPoints3DRed.Length, 12);
        computeBufferGreen = new ComputeBuffer(randomPoints3DGreen.Length, 12);
        computeBufferBlue = new ComputeBuffer(randomPoints3DBlue.Length, 12);
        computeBufferAlpha = new ComputeBuffer(randomPoints3DAlpha.Length, 12);
        computeBuffer.SetData(randomPoints3DRed);
        computeBufferGreen.SetData(randomPoints3DGreen);
        computeBufferBlue.SetData(randomPoints3DBlue);
        computeBufferAlpha.SetData(randomPoints3DAlpha);
    }

    private void InitializeRandomPoints()
    {
        randomPoints = new Vector2Int[numberOfSamplePoints * textureResolution];

        for (int i = 0; i < textureResolution; i++)
                for (int j = 0; j < numberOfSamplePoints; j++)
                    randomPoints[j + i * numberOfSamplePoints] = new Vector2Int(UnityEngine.Random.Range(0, textureResolution), UnityEngine.Random.Range(0, textureResolution));
    }

    private void InitializeRandomPoints3D()
    {
        randomPoints3DRed = new Vector3Int[numberOfSamplePoints * textureResolution * textureResolution];
        randomPoints3DGreen = new Vector3Int[numberOfSamplePoints * textureResolution * textureResolution];
        randomPoints3DBlue = new Vector3Int[numberOfSamplePoints * textureResolution * textureResolution];
        randomPoints3DAlpha = new Vector3Int[numberOfSamplePoints * textureResolution * textureResolution];

        for (int k = 0; k < textureResolution; k++)
            for (int i = 0; i < textureResolution; i++)
                    for (int j = 0; j < numberOfSamplePoints; j++)
                        randomPoints3DRed[j + i * numberOfSamplePoints + k * numberOfSamplePoints * numberOfSamplePoints] = new Vector3Int(UnityEngine.Random.Range(0, textureResolution), 
                            UnityEngine.Random.Range(0, textureResolution), UnityEngine.Random.Range(0, textureResolution));

        for (int k = 0; k < textureResolution; k++)
            for (int i = 0; i < textureResolution; i++)
                    for (int j = 0; j < numberOfSamplePoints; j++)
                        randomPoints3DGreen[j + i * numberOfSamplePoints + k * numberOfSamplePoints * numberOfSamplePoints] = new Vector3Int(UnityEngine.Random.Range(0, textureResolution), 
                            UnityEngine.Random.Range(0, textureResolution), UnityEngine.Random.Range(0, textureResolution));

        for (int k = 0; k < textureResolution; k++)
            for (int i = 0; i < textureResolution; i++)
                    for (int j = 0; j < numberOfSamplePoints; j++)
                        randomPoints3DBlue[j + i * numberOfSamplePoints + k * numberOfSamplePoints * numberOfSamplePoints] = new Vector3Int(UnityEngine.Random.Range(0, textureResolution), 
                            UnityEngine.Random.Range(0, textureResolution), UnityEngine.Random.Range(0, textureResolution));

        for (int k = 0; k < textureResolution; k++)
            for (int i = 0; i < textureResolution; i++)
                    for (int j = 0; j < numberOfSamplePoints; j++)
                        randomPoints3DAlpha[j + i * numberOfSamplePoints + k * numberOfSamplePoints * numberOfSamplePoints] = new Vector3Int(UnityEngine.Random.Range(0, textureResolution), 
                            UnityEngine.Random.Range(0, textureResolution), UnityEngine.Random.Range(0, textureResolution));
    }
}
