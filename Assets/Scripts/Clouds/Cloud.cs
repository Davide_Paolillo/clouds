﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode, ImageEffectAllowedInSceneView]
public class Cloud : MonoBehaviour
{
    [Header("Editor Settings")]
    [SerializeField] private Shader shader;
    [SerializeField] private Transform container;

    [Header("Animation Settings")]
    [SerializeField] private float timeScale;
    [SerializeField] private float baseSpeed;
    [SerializeField] private float detailSpeed;

    [Header("Shape Settings")]
    [SerializeField] private float scale;
    [SerializeField] private float detailNoiseScale;
    [SerializeField] private float detailNoiseWeight;
    [SerializeField] private Vector3 shapeOffset;
    [SerializeField] private Vector3 detailOffset;
    [SerializeField] private Vector3 detailWeights;
    [SerializeField] private Vector4 shapeNoiseWeights;
    [SerializeField] private Vector4 phaseParameters = Vector4.one;

    [Header("Light Settings")]
    [Range(0, 100)][SerializeField] private int numberOfStepsLight;
    [SerializeField] private float lightAbsorptionTowardSun;
    [SerializeField] private float lightAbsorptionThroughCloud;
    [SerializeField] private float darknessThreshold;

    [Header("Raymarch Settings")]
    [Range(0, 100)][SerializeField] private int numberOfSteps;
    [Range(0, 1000)][SerializeField] private float rayOffsetStrength;
    [SerializeField] private GameObject cloudObject;

    [Header("Cloud Settings")]
    [SerializeField] private float densityOffset;
    [SerializeField] private float densityMultiplier;
    //[Range(0f, 1f)][SerializeField] private float blendFactor;
    //[SerializeField] private float cloudScale;
    //[Range(0, 1)][SerializeField] private float densityThreshold;
    [SerializeField] private Vector3 cloudOffset;

    [Header("Debug Settings")]
    [SerializeField] private Material debugMaterial;
    [SerializeField] private GameObject debugGameObject;

    [SerializeField] private bool reload = true;

    private Texture3D noiseTexture;
    private Texture3D noiseTextureLowResolution;
    private Texture2D blueTexture;

    private RenderTexture rt;
    private RenderTexture rtLowRes;

    private Material material;

    private Camera _camera;

    private NoiseGeneratorComputeShader noiseGeneratorComputeShader;

    private void Start()
    {
        reload = true;
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        _camera = Camera.main;
        noiseGeneratorComputeShader = _camera.GetComponent<NoiseGeneratorComputeShader>();

        _camera.depthTextureMode = DepthTextureMode.Depth;

        Vector3 minBound = container.position - container.localScale / 2;
        Vector3 maxBound = container.position + container.localScale / 2;


        if (material == null || reload)
            material = new Material(shader);

        /*
        if (reload)
        {
            List<RenderTexture> rt = noiseGeneratorComputeShader.GenerateTextures2D(debugGameObject.transform.localScale);
            //var tex2D = noiseGeneratorComputeShader.FromRenderTextureToTexture2D(rt[Random.Range(0, 256)]);
            debugMaterial.mainTexture = rt[0];
        }
        */

        if (reload)
        {
            rt = noiseGeneratorComputeShader.GenerateTexture3D(cloudObject.transform.localScale);
            rtLowRes = noiseGeneratorComputeShader.GenerateTexture3D(cloudObject.transform.localScale, ResolutionDownscale.HALF);
            noiseTextureLowResolution = NoiseUtils.Get3DTexture(rtLowRes, noiseGeneratorComputeShader.Slicer, noiseGeneratorComputeShader.TextureResolution/2);
            noiseTextureLowResolution.Apply();
            noiseTexture = NoiseUtils.Get3DTexture(rt, noiseGeneratorComputeShader.Slicer, noiseGeneratorComputeShader.TextureResolution);
            noiseTexture.Apply();
        }

        RenderTexture tmp = noiseGeneratorComputeShader.GenerateTexture2D();
        blueTexture = NoiseUtils.FromRenderTextureToTexture2D(tmp, noiseGeneratorComputeShader.TextureResolution);

        material.SetInt("numberOfSteps", numberOfSteps);
        material.SetFloat("rayOffsetStrength", rayOffsetStrength);

        // Ignoring the rotations
        // The container center minus the radius gives us the container min bound
        material.SetVector("boundsMin", minBound);
        // The container center plus the radius gives us the container max bound
        material.SetVector("boundsMax", maxBound);

        // Setting the noise texture
        material.SetTexture("NoiseTexture", noiseTexture);
        material.SetTexture("NoiseTextureLowResolution", noiseTextureLowResolution);
        material.SetTexture("BlueNoise", blueTexture);

        // Shape settings
        material.SetFloat("scale", scale);
        material.SetFloat("detailNoiseScale", detailNoiseScale);
        material.SetFloat("detailNoiseWeight", detailNoiseWeight);
        material.SetVector("shapeOffset", shapeOffset);
        material.SetVector("detailOffset", detailOffset);
        material.SetVector("detailWeights", detailWeights);
        material.SetVector("shapeNoiseWeights", shapeNoiseWeights);
        material.SetVector("phaseParams", phaseParameters);

        // Clouds parameters
        material.SetVector("CloudOffset", cloudOffset);
        //material.SetFloat("CloudScale", cloudScale);
        //material.SetFloat("DensityThreshold", densityThreshold);
        material.SetFloat("densityOffset", densityOffset);
        material.SetFloat("DensityMultiplier", densityMultiplier);

        // Light settings
        material.SetInt("numberOfStepsLight", numberOfStepsLight);
        material.SetFloat("lightAbsorptionTowardSun", lightAbsorptionTowardSun);
        material.SetFloat("lightAbsorptionThroughCloud", lightAbsorptionThroughCloud);
        material.SetFloat("darknessThreshold", darknessThreshold);

        // Animation Settings
        material.SetFloat("timeScale", timeScale);
        material.SetFloat("baseSpeed", baseSpeed);
        material.SetFloat("detailSpeed", detailSpeed);

        reload = false;

        Graphics.Blit(source, destination, material);
    }
}
