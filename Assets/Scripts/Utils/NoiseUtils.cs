﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class NoiseUtils
{
    public static Texture3D FromRenderTexturesToTexture3D(List<RenderTexture> renderTextures, int textureResolution)
    {
        Texture3D output = new Texture3D(textureResolution, textureResolution, textureResolution, TextureFormat.ARGB32, true);

        output.filterMode = FilterMode.Trilinear;
        Color[] outputPixels = output.GetPixels();

        for (int k = 0; k < textureResolution; k++)
        {
            Color[] layerPixels = FromRenderTextureToTexture2D(renderTextures[k], textureResolution).GetPixels();
            for (int i = 0; i < textureResolution; i++)
                for (int j = 0; j < textureResolution; j++)
                    outputPixels[i + j * textureResolution + k * textureResolution * textureResolution] = layerPixels[i + j * textureResolution];
        }

        output.SetPixels(outputPixels);
        output.Apply();

        return output;
    }

    public static void Save(RenderTexture volumeTexture, string saveName, ComputeShader slicer, int textureResolution)
    {
#if UNITY_EDITOR
        float threadGroupSize = 32;
        string sceneName = UnityEditor.SceneManagement.EditorSceneManager.GetActiveScene().name;
        saveName = sceneName + "_" + saveName;
        int resolution = volumeTexture.width;
        Texture2D[] slices = new Texture2D[resolution];

        int slicerKernell = slicer.FindKernel("Slicer");

        slicer.SetInt("resolution", resolution);
        slicer.SetTexture(slicerKernell, "volumeTexture", volumeTexture);

        for (int layer = 0; layer < resolution; layer++)
        {
            var slice = new RenderTexture(resolution, resolution, 0);
            slice.dimension = UnityEngine.Rendering.TextureDimension.Tex2D;
            slice.enableRandomWrite = true;
            slice.Create();

            slicer.SetTexture(slicerKernell, "slice", slice);
            slicer.SetInt("layer", layer);
            int numThreadGroups = Mathf.CeilToInt(resolution / (float)threadGroupSize);
            slicer.Dispatch(slicerKernell, numThreadGroups, numThreadGroups, 1);

            slices[layer] = FromRenderTextureToTexture2D(slice, textureResolution);

        }

        var x = FromRenderTextureToTexture3D(slices, resolution);
        UnityEditor.AssetDatabase.CreateAsset(x, "Assets/Resources/" + saveName + ".asset");
#endif
    }

    public static Texture3D Get3DTexture(RenderTexture volumeTexture, ComputeShader slicer, int textureResolution)
    {
        float threadGroupSize = 32;
        int resolution = volumeTexture.width;
        Texture2D[] slices = new Texture2D[resolution];

        int slicerKernell = slicer.FindKernel("Slicer");

        slicer.SetInt("resolution", resolution);
        slicer.SetTexture(slicerKernell, "volumeTexture", volumeTexture);

        for (int layer = 0; layer < resolution; layer++)
        {
            var slice = new RenderTexture(resolution, resolution, 0);
            slice.dimension = UnityEngine.Rendering.TextureDimension.Tex2D;
            slice.enableRandomWrite = true;
            slice.Create();

            slicer.SetTexture(slicerKernell, "slice", slice);
            slicer.SetInt("layer", layer);
            int numThreadGroups = Mathf.CeilToInt(resolution / (float)threadGroupSize);
            slicer.Dispatch(slicerKernell, numThreadGroups, numThreadGroups, 1);

            slices[layer] = FromRenderTextureToTexture2D(slice, textureResolution);

        }

        Texture3D texture3D = FromRenderTextureToTexture3D(slices, resolution);

        return texture3D;
    }

    public static Texture3D Get3DTextureBlended(RenderTexture volumeTexture, RenderTexture lowResTexture, ComputeShader slicer, int textureResolution, float blendFactor)
    {
        float threadGroupSize = 32;
        int resolution = volumeTexture.width;
        Texture2D[] slices = new Texture2D[resolution];

        int slicerKernell = slicer.FindKernel("SlicerBlender");

        slicer.SetInt("resolution", resolution);
        slicer.SetFloat("blendFactor", blendFactor);

        slicer.SetTexture(slicerKernell, "volumeTexture", volumeTexture);
        slicer.SetTexture(slicerKernell, "volumeTextureLowRes", lowResTexture);

        for (int layer = 0; layer < resolution; layer++)
        {
            var slice = new RenderTexture(resolution, resolution, 0);
            slice.dimension = UnityEngine.Rendering.TextureDimension.Tex2D;
            slice.enableRandomWrite = true;
            slice.Create();

            slicer.SetTexture(slicerKernell, "slice", slice);
            slicer.SetInt("layer", layer);
            int numThreadGroups = Mathf.CeilToInt(resolution / (float)threadGroupSize);
            slicer.Dispatch(slicerKernell, numThreadGroups, numThreadGroups, 1);

            slices[layer] = FromRenderTextureToTexture2D(slice, textureResolution);

        }

        Texture3D texture3D = FromRenderTextureToTexture3D(slices, resolution);

        return texture3D;
    }

    public static Texture3D FromRenderTextureToTexture3D(Texture2D[] slices, int resolution)
    {
        Texture3D tex3D = new Texture3D(resolution, resolution, resolution, TextureFormat.ARGB32, false);
        tex3D.filterMode = FilterMode.Trilinear;
        Color[] outputPixels = tex3D.GetPixels();

        for (int z = 0; z < resolution; z++)
        {
            Color[] layerPixels = slices[z].GetPixels();
            for (int x = 0; x < resolution; x++)
                for (int y = 0; y < resolution; y++)
                    outputPixels[x + resolution * (y + z * resolution)] = layerPixels[x + y * resolution];
        }

        tex3D.SetPixels(outputPixels);
        tex3D.Apply();

        return tex3D;
    }

    public static Texture2D FromRenderTextureToTexture2D(RenderTexture renderTexture, int textureResolution)
    {
        RenderTexture.active = renderTexture;
        Texture2D texture2D = new Texture2D(textureResolution, textureResolution, TextureFormat.ARGB32, false);
        texture2D.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
        texture2D.Apply();
        RenderTexture.active = null;

        return texture2D;
    }
}
