﻿public enum ResolutionDownscale
{
    ZERO = 1,
    HALF = 2,
    QUARTER = 4,
}
