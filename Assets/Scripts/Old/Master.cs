﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Master : MonoBehaviour
{
    [SerializeField] private ComputeShader computeShader;

    [SerializeField] private GameObject box;

    [SerializeField] private Light sun;

    private Camera mainCamera;

    private RenderTexture target;

    private void Start()
    {
        mainCamera = Camera.main;
        mainCamera.depthTextureMode = DepthTextureMode.Depth;
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        InitializeRenderTexture();
        SetShaderParameters();

        computeShader.SetTexture(0, "Source", source);
        computeShader.SetTexture(0, "Destination", target);
        computeShader.SetTexture(0, "_CameraDepthTexture", Shader.GetGlobalTexture("_CameraDepthTexture"));

        int threadGroupX = Mathf.CeilToInt(mainCamera.pixelWidth/8.0f);
        int threadGroupY = Mathf.CeilToInt(mainCamera.pixelHeight / 8.0f);
        computeShader.Dispatch(0, threadGroupX, threadGroupY, 1);

        Graphics.Blit(target, destination);
    }

    private void InitializeRenderTexture()
    {
        if (target == null || target.width != mainCamera.pixelWidth || target.height != mainCamera.pixelHeight)
        {
            if (target != null)
                target.Release();

            target = new RenderTexture(mainCamera.pixelWidth, mainCamera.pixelHeight, 0, RenderTextureFormat.ARGBFloat, RenderTextureReadWrite.Linear);
            target.enableRandomWrite = true;
            target.Create();
        }
    }

    private void SetShaderParameters()
    {
        computeShader.SetMatrix("_CameraToWorld", mainCamera.cameraToWorldMatrix);
        computeShader.SetMatrix("_CameraInverseProjection", mainCamera.projectionMatrix.inverse);
        computeShader.SetVector("_Light", sun.transform.forward);
        computeShader.SetVector("_LightPosition", sun.transform.position);
        computeShader.SetVector("boxPosition", box.transform.position);
        computeShader.SetVector("boxSize", box.transform.localScale);
    }
}
