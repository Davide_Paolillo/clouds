﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class NoiseGenerator
{
    private const float MAX_DISTANCE = 30f;

    public static Texture3D GenerateTexturedNoise(int width, int height, int depth, int maxNumberOfRandomPoints)
    {
        Texture3D texture3D = new Texture3D(width, height, depth, TextureFormat.ARGB32, true);

        List<Vector3> randomPoints = new List<Vector3>();

        for (int z = 0; z < depth; z++)
        {
            for (int i = 0; i < maxNumberOfRandomPoints; i++)
            {
                int randomPointX = Random.Range(0, width);
                int randomPointY = Random.Range(0, height);

                randomPoints.Add(new Vector3(randomPointX, randomPointY, MAX_DISTANCE));
            }

            for (int y = 0; y < height; y++)
                for (int x = 0; x < width; x++)
                {
                    float minDistance = MAX_DISTANCE;

                    for (int i = 0; i < randomPoints.Count; i++)
                    {
                        float distance = Distance(randomPoints[i], new Vector3(x, y, z));

                        minDistance = Mathf.Min(minDistance, distance);
                    }

                    float lerpedValue = Mathf.InverseLerp(MAX_DISTANCE, 0, minDistance);
                    texture3D.SetPixel(x, y, z, new Color(lerpedValue, lerpedValue, lerpedValue));
                }

            randomPoints.Clear();
        }
            
        return texture3D;
    }

    public static Texture2D GenerateTexturedNoise2D(int width, int height, int depth, int maxNumberOfRandomPoints)
    {
        Texture2D texture2D = new Texture2D(width, height);

        List<Vector3> randomPoints = new List<Vector3>();

        for (int z = 0; z < 1; z++)
        {
            for (int i = 0; i < maxNumberOfRandomPoints; i++)
            {
                int randomPointX = Random.Range(0, width);
                int randomPointY = Random.Range(0, height);

                randomPoints.Add(new Vector3(randomPointX, randomPointY, MAX_DISTANCE));
            }

            for (int y = 0; y < height; y++)
                for (int x = 0; x < width; x++)
                {
                    float minDistance = MAX_DISTANCE;

                    for (int i = 0; i < randomPoints.Count; i++)
                    {
                        float distance = Distance(randomPoints[i], new Vector3(x, y, z));

                        minDistance = Mathf.Min(minDistance, distance);
                    }

                    float lerpedValue = Mathf.InverseLerp(MAX_DISTANCE, 0, minDistance);
                    texture2D.SetPixel(x, y, new Color(lerpedValue, lerpedValue, lerpedValue));
                }

            randomPoints.Clear();
        }
            
        return texture2D;
    }

    private static float Distance(Vector3 a, Vector3 b)
    {
        return Mathf.Sqrt(Mathf.Pow(b.x - a.x, 2) + Mathf.Pow(b.y - a.y, 2));
    }
}
