    *h          2019.4.22f1 ţ˙˙˙      ˙˙f!ë59Ý4QÁóB   í          7  ˙˙˙˙                 Ś ˛                       E                    Ţ  #                     . ,                     5   a                    Ţ  #                     . ,                      r                    Ţ  #      	               . ,      
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    ń  J   ˙˙˙˙    Ŕ           1  1  ˙˙˙˙                Ţ                        j  ˙˙˙˙                \     ˙˙˙˙                H r   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H w   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H    ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                      Ţ  #      !               . ,      "                   ˙˙˙˙#   @          1  1  ˙˙˙˙$               Ţ      %               . j     &               Ő    ˙˙˙˙'               1  1  ˙˙˙˙(    Ŕ            Ţ      )                  j  ˙˙˙˙*                H   ˙˙˙˙+               1  1  ˙˙˙˙,   @            Ţ      -                Q  j     .                y 
    /                 Ţ  #      0               . ,      1                 §      2    @            ž ś      3    @            Ţ  #      4               . ,      5               H ť   ˙˙˙˙6              1  1  ˙˙˙˙7   @            Ţ      8                Q  j     9                H Ć   ˙˙˙˙:              1  1  ˙˙˙˙;   @            Ţ      <                Q  j     =                H Ř   ˙˙˙˙>              1  1  ˙˙˙˙?   @            Ţ      @                Q  j     A              MonoImporter PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_ExternalObjects SourceAssetIdentifier type assembly name m_UsedFileIDs m_DefaultReferences executionOrder icon m_UserData m_AssetBundleName m_AssetBundleVariant     s    ˙˙ŁGń×ÜZ56 :!@iÁJ*          7  ˙˙˙˙                 Ś ˛                        E                    Ţ                       .                      (   a                    Ţ                       .                       r                    Ţ        	               .       
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    H ę ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     ń  =   ˙˙˙˙              1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               H   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                    Ţ                       .                      y Q                       Ţ                       .                       Ţ  X      !                H i   ˙˙˙˙"              1  1  ˙˙˙˙#   @            Ţ      $                Q  j     %                H u   ˙˙˙˙&              1  1  ˙˙˙˙'   @            Ţ      (                Q  j     )              PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_DefaultReferences m_Icon m_ExecutionOrder m_ClassName m_Namespace                      \       ŕyŻ     `                                                                                                                                                                               ŕyŻ                                                                                    Cloud   Š  using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode, ImageEffectAllowedInSceneView]
public class Cloud : MonoBehaviour
{
    [Header("Editor Settings")]
    [SerializeField] private Shader shader;
    [SerializeField] private Transform container;

    [Header("Animation Settings")]
    [SerializeField] private float timeScale;
    [SerializeField] private float baseSpeed;
    [SerializeField] private float detailSpeed;

    [Header("Shape Settings")]
    [SerializeField] private float scale;
    [SerializeField] private float detailNoiseScale;
    [SerializeField] private float detailNoiseWeight;
    [SerializeField] private Vector3 shapeOffset;
    [SerializeField] private Vector3 detailOffset;
    [SerializeField] private Vector3 detailWeights;
    [SerializeField] private Vector4 shapeNoiseWeights;
    [SerializeField] private Vector4 phaseParameters = Vector4.one;

    [Header("Light Settings")]
    [Range(0, 100)][SerializeField] private int numberOfStepsLight;
    [SerializeField] private float lightAbsorptionTowardSun;
    [SerializeField] private float lightAbsorptionThroughCloud;
    [SerializeField] private float darknessThreshold;

    [Header("Raymarch Settings")]
    [Range(0, 100)][SerializeField] private int numberOfSteps;
    [Range(0, 1000)][SerializeField] private float rayOffsetStrength;
    [SerializeField] private GameObject cloudObject;

    [Header("Cloud Settings")]
    [SerializeField] private float densityOffset;
    [SerializeField] private float densityMultiplier;
    [Range(0f, 1f)][SerializeField] private float blendFactor;
    [SerializeField] private float cloudScale;
    [Range(0, 1)][SerializeField] private float densityThreshold;
    [SerializeField] private Vector3 cloudOffset;

    [Header("Debug Settings")]
    [SerializeField] private Material debugMaterial;
    [SerializeField] private GameObject debugGameObject;


    [SerializeField] private bool reload = true;

    private bool reloadWithNewBlend = false;
    private float previousBlend = 0;

    private Texture3D noiseTexture;
    private Texture3D noiseTextureLowResolution;

    private RenderTexture rt;
    private RenderTexture rtLowRes;

    private Material material;

    private Camera _camera;

    private NoiseGeneratorComputeShader noiseGeneratorComputeShader;

    private void Start()
    {
        reload = true;
    }

    private void OnValidate()
    {
        if (previousBlend != blendFactor && rt != null && rtLowRes != null)
        {
            reloadWithNewBlend = true;
            previousBlend = blendFactor;
        }
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        _camera = Camera.main;
        noiseGeneratorComputeShader = _camera.GetComponent<NoiseGeneratorComputeShader>();

        _camera.depthTextureMode = DepthTextureMode.Depth;

        if (material == null)
            material = new Material(shader);

        Vector3 minBound = container.position - container.localScale / 2;
        Vector3 maxBound = container.position + container.localScale / 2;

        /*
        if (reload)
        {
            List<RenderTexture> rt = noiseGeneratorComputeShader.GenerateTextures2D(debugGameObject.transform.localScale);
            //var tex2D = noiseGeneratorComputeShader.FromRenderTextureToTexture2D(rt[Random.Range(0, 256)]);
            debugMaterial.mainTexture = rt[0];
        }*/

        if (reload)
        {
            rt = noiseGeneratorComputeShader.GenerateTexture3D(cloudObject.transform.localScale);
            rtLowRes = noiseGeneratorComputeShader.GenerateTexture3D(cloudObject.transform.localScale, ResolutionDownscale.HALF);
            noiseTextureLowResolution = NoiseUtils.Get3DTexture(rtLowRes, noiseGeneratorComputeShader.Slicer, noiseGeneratorComputeShader.TextureResolution);
            noiseTextureLowResolution.Apply();
            noiseTexture = NoiseUtils.Get3DTextureBlended(rt, rtLowRes, noiseGeneratorComputeShader.Slicer, noiseGeneratorComputeShader.TextureResolution, blendFactor);
            noiseTexture.Apply();
            reload = false;
        }
        else if (reloadWithNewBlend)
        {
            noiseTexture = NoiseUtils.Get3DTextureBlended(rt, rtLowRes, noiseGeneratorComputeShader.Slicer, noiseGeneratorComputeShader.TextureResolution, blendFactor);
            noiseTexture.Apply();
            reloadWithNewBlend = false;
        }

        material.SetInt("numberOfSteps", numberOfSteps);
        material.SetFloat("rayOffsetStrength", rayOffsetStrength);

        // Ignoring the rotations
        // The container center minus the radius gives us the container min bound
        material.SetVector("boundsMin", minBound);
        // The container center plus the radius gives us the container max bound
        material.SetVector("boundsMax", maxBound);

        // Setting the noise texture
        material.SetTexture("NoiseTexture", noiseTexture);
        material.SetTexture("NoiseTextureLowRes", noiseTextureLowResolution);
        material.SetTexture("BlueNoise", noiseGeneratorComputeShader.GenerateTexture2D());

        // Shape settings
        material.SetFloat("scale", scale);
        material.SetFloat("detailNoiseScale", detailNoiseScale);
        material.SetFloat("detailNoiseWeight", detailNoiseWeight);
        material.SetVector("shapeOffset", shapeOffset);
        material.SetVector("detailOffset", detailOffset);
        material.SetVector("detailWeights", detailWeights);
        material.SetVector("shapeNoiseWeights", shapeNoiseWeights);

        // Clouds parameters
        material.SetVector("CloudOffset", cloudOffset);
        material.SetFloat("CloudScale", cloudScale);
        material.SetFloat("DensityThreshold", densityThreshold);
        material.SetFloat("densityOffset", densityOffset);
        material.SetFloat("DensityMultiplier", densityMultiplier);

        // Light settings
        material.SetInt("numberOfStepsLight", numberOfStepsLight);
        material.SetFloat("lightAbsorptionTowardSun", lightAbsorptionTowardSun);
        material.SetFloat("lightAbsorptionThroughCloud", lightAbsorptionThroughCloud);
        material.SetFloat("darknessThreshold", darknessThreshold);

        // Animation Settings
        material.SetFloat("timeScale", timeScale);
        material.SetFloat("baseSpeed", baseSpeed);
        material.SetFloat("detailSpeed", detailSpeed);

        material.SetVector("phaseParams", phaseParameters);

        Graphics.Blit(source, destination, material);
    }
}
                          Cloud       